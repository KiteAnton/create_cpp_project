#include <gmock/gmock.h>

#include "SimpleClass.h"

using namespace ::testing;

namespace
{

    TEST(SimpleTest, FirstTest)
    {
        SimpleClass simpleClass{};

        bool simpleClassHasRun{simpleClass.run()};
        ASSERT_TRUE(simpleClassHasRun);
    }
}
