#include "SimpleClass.h"
#include <iostream>

bool SimpleClass::run() const
{
    std::cout << "SimpleClass running..." << "\n";
    return true;
}
