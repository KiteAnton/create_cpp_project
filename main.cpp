#include "SimpleClass.h"

int main(int argc, char *argv[])
{
    SimpleClass simpleClass{};

    simpleClass.run();
    return 0;
}
