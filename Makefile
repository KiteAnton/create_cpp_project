
LOCAL_BIN = ${HOME}/.local/bin
LOCAL_SHARE = ${HOME}/.local/share/create_cpp_project/


install:
	mkdir -p ${LOCAL_SHARE}
	cp CMakeLists.txt ${LOCAL_SHARE}
	cp main.cpp ${LOCAL_SHARE}
	cp Makefile ${LOCAL_SHARE}
	cp SimpleClass.cpp ${LOCAL_SHARE}
	cp SimpleClass.h ${LOCAL_SHARE}
	cp SimpleClass_test.cpp ${LOCAL_SHARE}
	chmod +x create_cpp_project
	cp create_cpp_project ${LOCAL_BIN}


